Para hoy:
- ACM sin variable ahorro (listo)
- Sacar los factores hasta la meseta y juntarlos con variable ahorro (listo)
- Aplicar Ward y hacer dendrograma (no se hace dendrograma pelotudos) (listo)
* Hay que tener también los dendrogramas de vecino más cercano y lejano (pelotudos)
- Hacer bella tabla con r2 pseudo-t y pseudo-f
- En principio boxplots por grupo para el ahorro, y ver si los demás tienen interpretación
- Ir a las variables originales y ver la frecuencia de quienes pertenencen a los grupos

Para después:
- Aplicar k-means o otro (usando cantidad de grupos sacada antes)
- Tabla con porcentaje de pertenencia a grupos de los individuos
- Ir a las variables originales y ver la frecuencia de quienes pertenencen a los grupos
- Hacer bello gráfico con los grupos en el plano

- Aplicar k-means o el método anterior directo en las variables originales
- Tabla con porcentaje de pertenencia a grupos de los individuos (finalizaría el primer análisis auxiliar)




